using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MOVER : MonoBehaviour
{
     Rigidbody rb;
     [SerializeField]float thrusting = 1000f;
     [SerializeField]float rotation = 1f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        playerthrust();
        playermoves();
    }

    void playerthrust ()
    {
        if(Input.GetKey(KeyCode.Space))
        {
            rb.AddRelativeForce(Vector3.up * thrusting * Time.deltaTime);
        }
    }
      void playermoves ()
    {
        if(Input.GetKey(KeyCode.A))
        {
            rotate(rotation);
        }

         if(Input.GetKey(KeyCode.D))
        {
            rotate(rotation);
        }

    }

     void rotate(float rotationthisframe )
     {
       transform.Rotate(Vector3.forward * rotationthisframe * Time.deltaTime);
     }
    
}




